﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaC05
{
    class Program
    {
        static void Main(string[] args)
        {
            Author author1 = new Author();
            Book book1 = new Book(); 

            author1.AddAtuhtor("Galaxyan Tom", "galaxyantom@gmail.com");
            book1.AddBook("How stars are counted", 2018, author1, 35);

            Console.WriteLine("Book " + book1.GetName() + " (" + book1.GetPrice() + "), by " + book1.GetAuthor().GetName() + ", published in " + book1.GetYear() + ".");

            Console.ReadKey();
        }
    }
}
