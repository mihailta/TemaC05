﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaC05
{
    public class Book
    {
        string name;
        int year;
        Author author = new Author();
        double price;

        public void AddBook(string name,int year, Author author, double price)
        {
            this.name = name;
            this.year = year;
            this.author = author;
            this.price = price;
        }

        public string GetName()
        {
            return name;
        }

        public Author GetAuthor()
        {
            return author;
        }

        public double GetPrice()
        {
            return price;
        }
          
        public int GetYear()
        {
            return year;
        }
    }
}
